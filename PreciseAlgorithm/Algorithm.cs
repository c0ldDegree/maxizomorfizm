﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PreciseAlgorithm
{
  public class Algorithm
    {
        bool[,] graphSmaller;
        bool[,] graphLarger;
        int availableEdgesCount;
        List<int> maxVerts1;
        List<int> maxVerts2;
        List<int> currentVerts1;
        List<int> currentVerts2;
        int min;
        int max;
        bool[] usedVertsFromLarger;
        bool[] usedVertsFromSmaller;
        int maxSize;
        int edgesCount;


        public Algorithm(string graphA, string graphB)
        {
            LoadGraphs(@"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_A_proszewski.csv", @"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_B_proszewski.csv");
        }

        public void Run()
        {
            Console.WriteLine("This is Precise Algorithm");
            maxVerts1 = new List<int>();
            maxVerts2 = new List<int>();
            maxSize = 0;

            //check which grahp has less vertices, create another matrix of edges for that graph
            min = Math.Min(graphSmaller.GetLength(0), graphLarger.GetLength(0)); 
            max = Math.Max(graphSmaller.GetLength(0), graphLarger.GetLength(0)); 
            usedVertsFromLarger = new bool[max];
            usedVertsFromSmaller = new bool[min];
            bool[,] smallerGraph;
            if (graphSmaller.GetLength(0) < graphLarger.GetLength(0))
                smallerGraph = graphSmaller;
            else
                smallerGraph = graphLarger;

            for (int i = 0; i < min; i++)
                for (int j = 0; j < i; j++)
                    if (smallerGraph[i, j])
                        availableEdgesCount++;
                    

            // create lists of vertices from both graphs representing currently analysed subgraph
            currentVerts1 = new List<int>();
            currentVerts2 = new List<int>();

            int index1 = 0;
            MSC_VE(index1);

            Console.WriteLine("Subgraph A:");
            Console.WriteLine(maxVerts1.ToString());
            Console.WriteLine("Subgraph B:");
            Console.WriteLine(maxVerts2.ToString());

        }

        private void LoadGraphs(string file1, string file2)
        {
            string[] filename1 = file1.Split('\\');
            string[] data = filename1[filename1.Length-1].Split('_');
            int.TryParse(data[0], out int size1);
            int.TryParse(data[1], out int size2);

            if ((size1 < size2) && (data[2] == "A") ||
                (size1 >= size2) && (data[2] == "B"))
            {
                    graphSmaller = GraphFromCSV(file1, size1);
                    graphLarger = GraphFromCSV(file2, size2);
            }
            else
            {
                graphLarger = GraphFromCSV(file1, size1);
                graphSmaller = GraphFromCSV(file2, size2);
            }

            /*
            using (var reader = new StreamReader(@"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_A_proszewski.csv"))
            {
                int col = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    for (int i = 0; i < values.Length; i++)
                        if (values[i] == "1")
                            graphSmaller[col, i] = true;
                    col++;
                }
            }
            using (var reader = new StreamReader(@"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_B_proszewski.csv"))
            {
                int col = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    for (int i = 0; i < values.Length; i++)
                        if (values[i] == "1")
                            graphLarger[col, i] = true;
                    col++;
                }
            }
            */
        }

        private bool[,] GraphFromCSV(string path, int size)
        {
            bool[,] graph = new bool[size, size];
            using (var reader = new StreamReader(path))
            {
                int col = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    for (int i = 0; i < values.Length; i++)
                        if (values[i] == "1")
                            graph[col, i] = true;
                    col++;
                }
            }
            return graph;
        }

        private void MSC_V(int index1)
        {
            //start checking verts of smaller graphs from index - previous verts have already been checked in previous iterations
            for(int i=index1; i<min; i++)
                for(int j=0; j<max; j++)
                {
                    //check if the vert grom larger graph has already been used
                    if (usedVertsFromLarger[j])
                        continue;
                    if (IsFeasiblePair(i, j))
                    {
                        //add pair of vertices to the common subgraph
                        AddPair_V(i, j);
                        //if the currently analysed subgraph if larger then MCS - save it as MCS
                        if (Size_V(currentVerts1) > maxSize && Connected(currentVerts1))
                            SaveCurrentMSC_V(currentVerts1, currentVerts2);
                        //check pruning condition
                        if ((min - index1) + Size_V(currentVerts1) > maxSize)
                            MSC_V(index1 + 1);
                        RemovePair_V(i, j);
                    }
                }
        }

        private void MSC_VE(int index1)
        {
            for (int i = index1+1; i < min; i++)
            {
                if (graphSmaller[i, index1])
                    availableEdgesCount--;
            }

            //start checking verts of smaller graphs from index - previous verts have already been checked in previous iterations
            for (int i = index1; i < min; i++)
                for (int j = 0; j < max; j++)
                {
                    //check if the vert grom larger graph has already been used
                    if (usedVertsFromLarger[j])
                        continue;
                    if (IsFeasiblePair(i, j))
                    {
                        //add pair of vertices to the common subgraph
                        AddPair_VE(i, j);
                        //if the currently analysed subgraph if larger then MCS - save it as MCS
                        if (Size_VE(currentVerts1) > maxSize && Connected(currentVerts1))
                            SaveCurrentMSC_VE(currentVerts1, currentVerts2);
                        //check pruning condition
                        if (availableEdgesCount + (min - index1) + Size_VE(currentVerts1) > maxSize)
                            MSC_VE(index1 + 1);
                        RemovePair_VE(i, j);
                    }
                }

            for (int i = index1 + 1; i < min; i++)
            {
                if (graphSmaller[i,index1])
                    availableEdgesCount++;
            }
        }
        

        private bool IsFeasiblePair(int index1, int index2)
        {
            for (int i=0; i<currentVerts1.Count; i++)
            {
                // there must exist an exact mapping of edges between two graphs
                if ((graphSmaller[currentVerts1[i], index1] == true && graphLarger[currentVerts2[i], index2] == false) ||
                    (graphSmaller[currentVerts1[i], index1] == false && graphLarger[currentVerts2[i], index2] == true))
                    return false;
            }
            return true;
        }
        
        private void AddPair_V(int index1, int index2)
        {
            currentVerts1.Add(index1);
            currentVerts2.Add(index2);
            usedVertsFromLarger[index2] = true;
        }

        private void AddPair_VE(int index1, int index2)
        {
            currentVerts1.Add(index1);
            currentVerts2.Add(index2);

            usedVertsFromLarger[index2] = true;
            usedVertsFromSmaller[index1] = true;

            for (int i = 0; i < index1; i++)
            {
                if (usedVertsFromSmaller[i] && graphSmaller[index1, i])
                    edgesCount++;
            }
        }

        private void RemovePair_V(int index1, int index2)
        {
            currentVerts1.RemoveAt(currentVerts1.Count - 1);
            currentVerts2.RemoveAt(currentVerts2.Count - 1);
            usedVertsFromLarger[index2] = false;
        }

        private void RemovePair_VE(int index1, int index2)
        {
            currentVerts1.RemoveAt(currentVerts1.Count - 1);
            currentVerts2.RemoveAt(currentVerts2.Count - 1);

            usedVertsFromLarger[index2] = false;
            usedVertsFromSmaller[index1] = false;

            for (int i = 0; i < index1; i++)
            {
                if (usedVertsFromSmaller[i] && graphSmaller[index1, i])
                    edgesCount--;
            }
        }

        private bool Connected(List<int> sub1)
        {
            bool[,] smallerGraph;
            if (graphSmaller.GetLength(0) < graphLarger.GetLength(0))
                smallerGraph = graphSmaller;
            else
                smallerGraph = graphLarger;

            // 0 = not in subgraph, 1 = not visited, 2 = visited
            int[] vertsTab = new int[min];
            foreach (var v in sub1)
                vertsTab[v] = 1;
            int visited = 0;
            Queue<int> vertices = new Queue<int>();
            int i = 0;
            while (vertsTab[i] == 0 && i < min)
                i++;
            vertices.Enqueue(i);

            while (vertices.Count != 0)
            {
                int vert = vertices.Dequeue();
                for(i=0; i<min; i++)
                    if(smallerGraph[vert,i] && vertsTab[i] == 1)
                    {
                        vertices.Enqueue(i);
                        vertsTab[i] = 2;
                        visited++;
                    }
            }
            return (visited == sub1.Count);
        }

        private int Size_V(List<int> graph)
        {
            return graph.Count;
        }

        private int Size_VE(List<int> graph)
        {
            return graph.Count + edgesCount;
        }

        private void SaveCurrentMSC_V(List<int> sub1, List<int> sub2)
        {
            maxVerts1 = new List<int>(sub1);
            maxVerts2 = new List<int>(sub2);
            maxSize = Size_V(sub1);
        }

        private void SaveCurrentMSC_VE(List<int> sub1, List<int> sub2)
        {
            maxVerts1 = new List<int>(sub1);
            maxVerts2 = new List<int>(sub2);
            maxSize = Size_VE(sub1);
        }
    }
}
