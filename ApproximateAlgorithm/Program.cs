﻿using System;

namespace ApproximateAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            Algorithm alg = new Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_8_A_proszewski.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_8_B_proszewski.csv");
            Algorithm test_3_3 = new Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\3_3_A_korecki.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\3_3_B_korecki.csv");
            Algorithm test_7_7 = new Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_7_A_korecki.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_7_B_korecki.csv");
            Algorithm test_8_7 = new Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\8_7_A_korecki.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\8_7_B_korecki.csv");
            Algorithm test_9_9 = new Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\9_9_A_korecki.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\9_9_B_korecki.csv");
            alg.Run();
            alg.VplusE = true;
            alg.Run();
            test_3_3.Run();
            test_3_3.VplusE = true;
            test_3_3.Run();
            test_7_7.Run();
            test_7_7.VplusE = true;
            test_7_7.Run();
            test_8_7.Run();
            test_8_7.VplusE = true;
            test_8_7.Run();
            test_9_9.Run();
            test_9_9.VplusE = true;
            test_9_9.Run();
        }
    }
}
