﻿using System;

namespace MaxIzomorfizm
{
    class Program
    {
        static void Main(string[] args)
        {
            ApproximateAlgorithm.Algorithm aproxAlg = new ApproximateAlgorithm.Algorithm(@"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_8_A_proszewski.csv", @"C:\Users\Jarek\source\repos\maxizomorfizm\ApproximateAlgorithm\star\7_8_B_proszewski.csv");
            PreciseAlgorithm.Algorithm precAlg = new PreciseAlgorithm.Algorithm();

            aproxAlg.Run();
           // precAlg.Run();
        }
    }
}
