﻿using System;

namespace PreciseAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            Algorithm alg = new Algorithm(@"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_A_proszewski.csv", @"C:\Users\HP\Documents\VisualStudio2017\Projects\maxizomorfizm\ApproximateAlgorithm\star\7_8_B_proszewski.csv");
            alg.Run();
        }
    }
}
