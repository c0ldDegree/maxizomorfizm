﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
namespace ApproximateAlgorithm
{
    public class Algorithm
    {
        private readonly string path_A;
        private readonly string path_B;
        private Tuple<List<int>, Dictionary<int, HashSet<int>>> result_A;
        private Tuple<List<int>, Dictionary<int, HashSet<int>>> result_B;
        public List<int> A_maxIzo;
        public List<int> B_maxIzo;
        private Dictionary<int, HashSet<int>> A_list;
        private Dictionary<int, HashSet<int>> B_list;
        private HashSet<int> A_izo_set;
        private HashSet<int> B_izo_set;
        public bool VplusE;
        public Algorithm(string path_A, string path_B,bool VplusE=false)
        {
            this.path_A = path_A;
            this.path_B = path_B;
            A_maxIzo = new List<int>();
            B_maxIzo = new List<int>();
            result_A = new Tuple<List<int>, Dictionary<int, HashSet<int>>>(new List<int>(), new Dictionary<int, HashSet<int>>());
            result_B = new Tuple<List<int>, Dictionary<int, HashSet<int>>>(new List<int>(), new Dictionary<int, HashSet<int>>());
            A_list = new Dictionary<int, HashSet<int>>();
            B_list = new Dictionary<int, HashSet<int>>();
            A_izo_set = new HashSet<int>();
            B_izo_set = new HashSet<int>();
            this.VplusE = VplusE;
        }

        public void Run()
        {
            var A = Csv2Graphs(path_A);
            var B = Csv2Graphs(path_B);
            FindMaxIzo(A, B);
            Console.WriteLine(this.ToString());
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Maksymalność ze względu na "+ (VplusE ?"V+E" :"V")).AppendLine();
            sb.Append("Podgraf grafu A: ").AppendLine();
            foreach (var i in A_maxIzo)
            {
                //sb.Append(item.Key + "->");
                //foreach (var value in item.Value)
                //{
                //    sb.Append(value + ",");
                //}
                //sb.Remove(sb.Length - 1, 1);
                //sb.AppendLine();
                sb.Append(i + ",");
                sb.Remove(sb.Length - 1, 1);
            }
            sb.AppendLine();
            sb.Append("Podgraf grafu B: ").AppendLine();
            foreach (var i in B_maxIzo)
            {
                //sb.Append(item.Key + "->");
                //foreach (var value in item.Value)
                //{
                //    sb.Append(value + ",");
                //}
                //sb.Remove(sb.Length - 1, 1).AppendLine();
                sb.Append(i + ",");
                sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }
        public Tuple<List<List<bool>>, Dictionary<int, HashSet<int>>> Csv2Graphs(string path)
        {
            List<List<bool>> A = new List<List<bool>>();
            Dictionary<int, HashSet<int>> dict = new Dictionary<int, HashSet<int>>();
            using (StreamReader textReader = File.OpenText(path))
            {
                var csv = new CsvReader(textReader);
                csv.Configuration.HasHeaderRecord = false;
                string value;
                int i = 0;
                while (csv.Read())
                {

                    A.Add(new List<bool>());
                    HashSet<int> vertexSet = new HashSet<int>();
                    for (int j = 0; csv.TryGetField<string>(j, out value); j++)
                    {
                        A[i].Add(int.Parse(value) != 0);
                        if (A[i][j] && i != j)
                            vertexSet.Add(j);
                    }
                    dict.Add(i, vertexSet);
                    i++;
                }
            }
            return new Tuple<List<List<bool>>, Dictionary<int, HashSet<int>>>(A, dict);
        }
        private void FindMaxIzo(Tuple<List<List<bool>>, Dictionary<int, HashSet<int>>> A, Tuple<List<List<bool>>, Dictionary<int, HashSet<int>>> B)//O(n*m*n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n))
        {
            List<List<bool>> A_matrix = A.Item1;
            List<List<bool>> B_matrix = B.Item1;
            A_list = A.Item2;
            B_list = B.Item2;
            List<int> A_izo, B_izo;
            int[] A_degrees = CalculateDegrees(A_list);
            int[] B_degrees = CalculateDegrees(B_list);
            SortedDictionary<int, int> A_vertex_degree_sorted = new SortedDictionary<int, int>(new IntReverseComparer(A_degrees));
            SortedDictionary<int, int> B_vertex_degree_sorted = new SortedDictionary<int, int>(new IntReverseComparer(B_degrees));
            int u, v;
            for (int i = 0; i < A_matrix.Count; i++)//n*m*n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
                for (int j = 0; j < B_matrix.Count; j++)//m*n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
                {
                    u = i;
                    v = j;
                    A_izo = new List<int>();
                    B_izo = new List<int>();
                    BuildIzomorfizm(u, v, A_izo, B_izo, A_vertex_degree_sorted, B_vertex_degree_sorted);//n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
                    if ((!VplusE && A_izo.Count > A_maxIzo.Count) || (VplusE && A_izo.Count + Edges(A_izo, A_list) > A_maxIzo.Count+Edges(A_maxIzo,A_list)))
                    {
                        A_maxIzo = A_izo;
                        B_maxIzo = B_izo;
                    }
                }
        }
        private void BuildIzomorfizm(int u, int v, List<int> A_izo, List<int> B_izo, SortedDictionary<int, int> A_vertex_degree_sorted,
            SortedDictionary<int, int> B_vertex_degree_sorted)//O(n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n))
        {
            while (true)//n*(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
            {
                A_izo.Add(u);
                B_izo.Add(v);
                A_izo_set.Add(u);
                B_izo_set.Add(v);
                if (!NextPair(u, v, A_izo, B_izo, A_vertex_degree_sorted, B_vertex_degree_sorted, out int new_u, out int new_v))//(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
                {
                    A_vertex_degree_sorted.Clear();
                    B_vertex_degree_sorted.Clear();
                    A_izo_set.Clear();
                    B_izo_set.Clear();
                    return;
                }
                u = new_u;
                v = new_v;
                A_vertex_degree_sorted.Clear();
                B_vertex_degree_sorted.Clear();
            }
        }
        private bool NextPair(int u, int v, List<int> A_izo, List<int> B_izo, SortedDictionary<int, int> A_vertex_degree_sorted,
            SortedDictionary<int, int> B_vertex_degree_sorted, out int new_u, out int new_v)//O(log(n-1)+log(m-1)+log(n-1)*log(m-1)*n)
        {
            foreach (var u_neighbor in this.A_list[u])//log(n-1)
                A_vertex_degree_sorted.Add(u_neighbor, u_neighbor);//zmienic na SortedList, wartosc jest zupelnie niepotrzebna
            foreach (var v_neighbor in B_list[v])//log(m-1)
                B_vertex_degree_sorted.Add(v_neighbor, v_neighbor);
            foreach (var u_n in A_vertex_degree_sorted)//log(n-1)*log(m-1)*n
                foreach (var v_n in B_vertex_degree_sorted)//log(m-1)*n
                {
                    bool checkIzo = true;
                    if (A_izo_set.Contains(u_n.Key) || B_izo_set.Contains(v_n.Key))//1
                        continue;
                    for (int i = 0; i < A_izo.Count; i++)// max n elementów
                        if ((A_list[u_n.Key].Contains(A_izo[i]) || B_list[v_n.Key].Contains(B_izo[i])) &&
                           ((A_list[u_n.Key].Contains(A_izo[i]) && !B_list[v_n.Key].Contains(B_izo[i])) ||
                            (B_list[v_n.Key].Contains(B_izo[i]) && !A_list[u_n.Key].Contains(A_izo[i]))))
                        {
                            checkIzo = false;
                            break;
                        }
                    if (checkIzo)
                    {
                        new_u = u_n.Key;
                        new_v = v_n.Key;
                        return true;
                    }
                }
            new_u = -1;
            new_v = -1;
            return false;
        }
        private int[] CalculateDegrees(Dictionary<int, HashSet<int>> graph)
        {
            int[] degrees = new int[graph.Count];
            foreach (var v in graph)
                degrees[v.Key] = v.Value.Count;
            return degrees;
        }
        private int Edges(List<int> izo, Dictionary<int, HashSet<int>> G_list)
        {
            int sum = 0;
            foreach(var v in izo)
            {
                foreach(var u in G_list[v])
                {
                    if (izo.Contains(u))
                    {
                        sum++;
                    }
                }
            }
            return sum / 2;
        }
        class IntReverseComparer : IComparer<int>
        {
            readonly int[] A;
            public IntReverseComparer(int[] degree) : base()
            {
                A = degree;
            }
            public int Compare(int x, int y)
            {
                if (A[y].CompareTo(A[x]) == 0)
                    return x.CompareTo(y);
                else
                    return A[y].CompareTo(A[x]);
            }
        }
    }
}
